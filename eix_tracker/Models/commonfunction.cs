﻿/*************************************************************************
* 
* EIX SYSTEMS CONFIDENTIAL
* __________________
* 
*  [2010] - [2016] EIX SYSTEMS LLC 
*  All Rights Reserved.
* 
* NOTICE:  All information contained herein is, and remains
* the property of EIX Systems LLC and its suppliers,
* if any.  The intellectual and technical concepts contained
* herein are proprietary to EIX Systems LLC
* and its suppliers and may be covered by U.S. and Foreign Patents,
* patents in process, and are protected by trade secret or copyright law.
* Dissemination of this information or reproduction of this material
* is strictly forbidden unless prior written permission is obtained
* from EIX Systems LLC.
/*--------------------------------------------------------
* File Name....: CommonRoutines.cs
* Object Type..: C# Class
* Namespace....: Ride.Common
* Framework....: 4.5.2
* File Version.: 1.0.0
* Create Date..: 02-06-2015
* -------------------------------------------------------
* Company......: EIX SYSTEMS, LLC
* Author.......: Nirav Gohel
* Email........: nirav.gohel@eixsys.com
* Webpage......: www.eixsys.com
* Description..: 
* Notes........: 
* -------------------------------------------------------*/
#region Revisions
/*--------------------------------------------------------
* Revisions....: 
* Author.......: 
* Date.........:  
* Description..: 
* Notes........: 
* -------------------------------------------------------*/
#endregion

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.IO;
using System.Net;
using System.Xml;
using System.Xml.Xsl;
using System.Xml.XPath;
using System.Xml.Linq;
using System.Xml.Schema;
using Newtonsoft.Json;
using System.Data;
using System.Drawing;
using System.ComponentModel;
using System.Security.Cryptography;

namespace Healthcare_API.Models
{
    public enum MessageType
    {
        X12,
        XML,
        HL7,
        NAA
    }

    public enum LoggerType
    {
        IN,
        OUT,
        STAT,
        NAA
    }

    public enum VrfMethod
    {

        EMAIL,
        PHONE
    }

    public class CommonRoutines
    {
        ~CommonRoutines()
        {
            Dispose(false);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {

            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


        public string CreateNewGUID()
        {
            string strGUID;

            try
            {
                System.Guid strSysGUID = System.Guid.NewGuid();
                strGUID = strSysGUID.ToString().Replace("-", "");

                return strGUID;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public int getRandomID()

        {
            Random r = new Random();
            return r.Next(10000, 99999);

        }

        public string DataTableToJSONWithJSONNet(DataTable table)
        {
            string JSONString = string.Empty;
            JSONString = JsonConvert.SerializeObject(table);
            return JSONString;
        }

        public DataTable ToDataTable<T>(List<T> iList)
        {
            DataTable dataTable = new DataTable();
            PropertyDescriptorCollection propertyDescriptorCollection =
                TypeDescriptor.GetProperties(typeof(T));
            for (int i = 0; i < propertyDescriptorCollection.Count; i++)
            {
                PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                Type type = propertyDescriptor.PropertyType;

                if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
                    type = Nullable.GetUnderlyingType(type);


                dataTable.Columns.Add(propertyDescriptor.Name, type);
            }
            object[] values = new object[propertyDescriptorCollection.Count];
            foreach (T iListItem in iList)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = propertyDescriptorCollection[i].GetValue(iListItem);
                }
                dataTable.Rows.Add(values);
            }
            return dataTable;
        }
    }
}

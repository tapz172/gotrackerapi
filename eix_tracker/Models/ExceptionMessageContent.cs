﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using System.Text;

namespace eix_tracker.Models
{
    public class ExceptionMessageContent
    {
        public string Error { get; set; }
        public string Message { get; set; }
    }
    public class PdfData
    {
        public string data { get; set; }
    }

}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class Client
    {
        public string location_name { get; set; }
        public string address_one { get; set; }
        public string address_two { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string location_status { get; set; }
        public DateTime entry_date { get; set; }
        public string entry_user { get; set; }
        public int zone_id { get; set; }
        public int userid { get; set; }
        public string QR_code { get; set; }
        public string location_type { get; set; }
        public string start_limit { get; set; }
        public string search { get; set; }
        public string search_string { get; set; }

        public string user_id { get; set; }
        public string role_id { get; set; }
        public string location_id { get; set; }
        public string client_id { get; set; }
    }
}

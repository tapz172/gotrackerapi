﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class requestAllApi
    {
        public string api_name { get; set; }
        public string api_method { get; set; }
        public int user_id { get; set; }
    }
}

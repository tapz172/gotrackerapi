﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class Zone
    {
        public string Zone_Name { get; set; }
        public DateTime update_date { get; set; }

        public string user_id { get; set; }
        public string zoneid { get; set; }
    }
}

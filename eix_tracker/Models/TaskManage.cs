﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class TaskManage
    {
        public string Description { get; set; }
        public string task_status { get; set; }
        public string task_frequency { get; set; }
        public string entry_date { get; set; }
        public string Task_Name { get; set; }
        public string Task_Assigment { get; set; }
        public string task_time_duration { get; set; }
        public string allocate_to { get; set; }
        public string userid { get; set; }
        public string clientid { get; set; }
        public string locationid { get; set; }
        public string roleid { get; set; }
        public string taskid { get; set; }
        //public int questionid { get; set; }
        public string serviceId { get; set; }
        public string start_limit { get; set; }
        public string search { get; set; }
        public string search_string { get; set; }
    }

    public class add_location_service
    {
        public int serviceid { get; set; }
        public int userid { get; set; }
        public int locationid { get; set; }
        public string service_name { get; set; }
        public string service_status { get; set; }
        public string entry_date { get; set; }
    }
    public class sms_add_service_question
    {
        public int questionid { get; set; }
        public int userid { get; set; }
        public int service_id { get; set; }
        public string question_text { get; set; }
        public string question_status { get; set; }
        public string entry_date { get; set; }
    }
    public class subscription_status_change
    {
        public string userid { get; set; }
        public string sub_status { get; set; }
        public string close_date { get; set; }
        public string last_date { get; set; }
    }

}

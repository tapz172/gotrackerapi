﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class attachmentCollection
    {  
        public List<attachment> arrAttachment { get; set; }
    }
    public class attachment
    {
        public int report_detail_id { get; set; }
        public string ext { get; set; }
        public string attachment_type { get; set; }
        public string attachment_name { get; set; }
        public DateTime entry_date { get; set; }
    }
    public class StudentModel
    {
        public string Name { get; set; }
        public IFormFile Image { get; set; }
    }
    public class attachmentfor_andrd_ios
    {
        public string report_detail_id { get; set; }
        public IFormFile File { get; set; }
        public string entry_date { get; set; }
        public string attachment_type { get; set; }
    }
}

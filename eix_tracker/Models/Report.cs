﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class Report
    {
        public string report_type { get; set; }
        public string entry_date { get; set; }
        public string report_desc { get; set; }
        //public int Location_id { get; set; }
        //public string service_id { get; set; }
        public string report_detail_id { get; set; }
        public int user_id { get; set; }
        public int locationid { get; set; }
        public int report_id { get; set; }
        public int roleid { get; set; }
        public int clientid { get; set; }
        public string task_id { get; set; }
        public string lat_upd { get; set; }
        public string longitude_upd { get; set; }

    }

    public class Report_detail_pdf
    {
        public int report_id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string address_one { get; set; }
        public string address_two { get; set; }
        public string report_desc { get; set; }
        public string comp_name { get; set; }
        public string report_type { get; set; }
        public string task_name { get; set; }
        public string user_added_by_role_name { get; set; }
        public string security_company_addressone { get; set; }
        public string security_company_mobile { get; set; }
        public string location { get; set; }
        public string user_by { get; set; }
        public string report_date { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
    }

    public class services_detail_pdf
    {
        public string services_name { get; set; }
        public string question_text { get; set; }
        public string answ_comment { get; set; }
    }


    public class add_report_service_que_answ
    {
        public string report_id_answ { get; set; }
        public List<services> _services { get; set; }
    }

    public class services
    {
        public string report_id_answ { get; set; }
        public int services_id { get; set; }
        public int qus_id { get; set; }
        public string answer { get; set; }
        public string comments { get; set; }
        //public List<attachment_ques> arrAttachment { get; set; }
        public string img_ext { get; set; }
        public string vdo_ext { get; set; }
        public string attachment_type { get; set; }
        public string answ_image { get; set; }
        public string answ_video { get; set; }
        public DateTime entry_date { get; set; }
        public string imageUpdate { get; set; }
        public string vidioUpdate { get; set; }
        //public List<questions> _questions { get; set; }
    }

    public class questions
    {
        public int qus_id { get; set; }
        public string answer { get; set; }
        public string comments { get; set; }
        //public List<attachment_ques> arrAttachment { get; set; }
        public string img_ext { get; set; }
        public string vdo_ext { get; set; }
        public string attachment_type { get; set; }
        public string answ_image { get; set; }
        public string answ_video { get; set; }
        public DateTime entry_date { get; set; }
    }

    public class attachment_ques
    {
        //public int report_detail_id { get; set; }
       
    }
}

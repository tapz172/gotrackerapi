﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using System.Data;
using MySql.Data.MySqlClient;


namespace eix_tracker.Models
{
    public class Login
    {
        [Required]
        public string username { get; set; }
        [Required]
        public string password { get; set; }
        public string public_ip { get; set; }
        public string device_id { get; set; }
        public string browser_desc { get; set; }
        public string os_desc { get; set; }
        public int UserID { get; set; }
        public int login_attempts { get; set; }
        public string lat_upd { get; set; }
        public string longitude_upd { get; set; }
    }
    public class Login_Attempt
    {
        public string email_id { get; set; }
        public int login_attempt { get; set; }
    }
}

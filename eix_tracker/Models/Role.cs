﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class Role
    {
        //public int Role_ID { get; set; }
        public string Role_Name { get; set; }
        public int access_ID { get; set; }
        public int role_add { get; set; }
        public int role_update { get; set; }
        public int role_delete { get; set; }
        public int role_view { get; set; }
        public string Update_Date { get; set; }
        public string Role_Id { get; set; }
        public virtual ICollection<register> reg { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class add_payment
    {
        public string user_id { get; set; }
        public string role_id { get; set; }
        public string pay_card_no { get; set; }
        public string pay_expire_month { get; set; }
        public string pay_expire_year { get; set; }
        public string pay_total_amount { get; set; }
        public string pay_state_tax { get; set; }
        public string pay_status { get; set; }
        public string entry_date { get; set; }
        public string entry_user { get; set; }
        public string CustomerId { get; set; }
        public string SubscriptionId { get; set; }
        public string payment_date { get; set; }
        public string send_first_payment_status { get; set; }
        public string get_total_guard { get; set; }
        public string upgrade_plan { get; set; }
        public string old_amount { get; set; }

    }
    public class sms_add_guard_location
    {
        public string user_id { get; set; }
        public string security_comp_id { get; set; }
        public string guard_username { get; set; }
        public string lat { get; set; }
        public string longitude { get; set; }
    }
}

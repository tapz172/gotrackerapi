﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class register
    {
        public int user_id { get; set; }
        public string username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email_ID { get; set; }
        public string Phone_No { get; set; }
        public string Password { get; set; }
        public string New_Password { get; set; }
        public DateTime Entry_Date { get; set; }
        public DateTime Update_Date { get; set; }
        public string user_password_status { get; set; }
        public int role_Id { get; set; }
        public string Comp_Name { get; set; }
        public string Comp_Type { get; set; }
        public string Comp_status { get; set; }
        public string Address_One { get; set; }
        public string Address_Two { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string license_state { get; set; }
        public string dps_no { get; set; }
        public string rndnumber { get; set; }
        public string Client_Name { get; set; }
        public string Comp_ID { get; set; }
        public string Image { get; set; }
        public string ImageContent { get; set; }
        public string emailaddr { get; set; }
        public string ext { get; set; }
        public string user_key { get; set; }
        public string imageUpdate { get; set; }
        public string plan_amount { get; set; }
        public string plan_desc { get; set; }
        public string pic { get; set; }
        public string total_guard { get; set; }
        public string lat { get; set; }
        public string longitude { get; set; }
        public string role_id { get; set; }
        public string start_limit { get; set; }
        public string client_identify { get; set; }
        //public virtual ICollection<Role> role { get; set; }
        public string search { get; set; }
        public string search_string { get; set; }
    }

    public class registerbyID
    {
        //public int profile_insert_id { get; set; }
        public string username { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email_ID { get; set; }
        public string Phone_No { get; set; }
        public string role_name { get; set; }
        public string Comp_Name { get; set; }
        public string Comp_Type { get; set; }
        public string Comp_status { get; set; }
        public string Address_One { get; set; }
        public string Address_Two { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string License_state { get; set; }
        public string Dps_no { get; set; }


    }
    public class changestatus
    {
        public string userid { get; set; }
        public string change_status { get; set; }
        public string inactive_date { get; set; }
    }
}

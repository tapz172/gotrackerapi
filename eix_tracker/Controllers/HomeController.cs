﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System.Web.Cors;
using Microsoft.AspNetCore.Diagnostics;

namespace eix_tracker.Controllers
{
    [ResponseCache(VaryByHeader = "User-Agent", Duration = 30)]
    public class HomeController : Controller
    {
        //private readonly IMemoryCache memoryCache;

        //public HomeController(IMemoryCache memoryCache)
        //{
        //    this.memoryCache = memoryCache;
        //}
        private readonly ILogger<HomeController> _logger;
        //public HomeController(ILogger<HomeController> logger)
        //{
        //    _logger = logger;
        //}
        IMemoryCache _memoryCache;
        public HomeController(IMemoryCache memoryCache, ILogger<HomeController> logger)
        {
            _logger = logger;
            _memoryCache = memoryCache;
        }

        public IActionResult Index()
        {
            _logger.LogInformation("Index page says hello");

            var mvcName = typeof(Controller).Assembly.GetName();
            var isMono = Type.GetType("Mono.Runtime") != null;

            ViewData["Version"] = mvcName.Version.Major + "." + mvcName.Version.Minor;
            ViewData["Runtime"] = isMono ? "Mono" : ".NET";

            return View();
            //return View();
        }
        
        [Route("home/SetCacheData")]
        public IActionResult SetCacheData()
        {
            //var Time = DateTime.Now.ToLocalTime().ToString();
            //MemoryCacheEntryOptions cacheOption = new MemoryCacheEntryOptions()
            //{
            //    AbsoluteExpirationRelativeToNow = (DateTime.Now.AddMinutes(1) - DateTime.Now)
            //};
            //_memoryCache.Set("Time", Time, cacheOption);
            //return View();
            DateTime currentTime;
            bool isExist = _memoryCache.TryGetValue("CacheTime", out currentTime);
            if (!isExist)
            {
                currentTime = DateTime.Now;
                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromSeconds(30));

                _memoryCache.Set("CacheTime", currentTime, cacheEntryOptions);
            }
            return View(currentTime);
        }
        [Route("home/GetCacheData")]
        public IActionResult GetCacheData()
        {
            _memoryCache.Remove("CacheTime");
            DateTime? currentTime = _memoryCache.Get<DateTime?>("CacheTime");
            return View("SetCacheData", currentTime);
        }
        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";
            return View();
        }

        public IActionResult Error()
        {
            // Get the details of the exception that occurred
            var exceptionFeature = HttpContext.Features.Get<IExceptionHandlerPathFeature>();

            if (exceptionFeature != null)
            {
                // Get which route the exception occurred at
                string routeWhereExceptionOccurred = exceptionFeature.Path;

                // Get the exception that occurred
                Exception exceptionThatOccurred = exceptionFeature.Error;
                // TODO: Do something with the exception
                // Log it with Serilog?
                // Send an e-mail, text, fax, or carrier pidgeon?  Maybe all of the above?
                // Whatever you do, be careful to catch any exceptions, otherwise you'll end up with a blank page and throwing a 500
            }
            //return View();
            return View("Error",new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}

﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using eix_tracker.Models;
using Newtonsoft.Json;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace WebApiAuthDemo.Controllers
{
    public class TokenController : Controller
    {
        private readonly IConfiguration _configuration;
        private readonly ILogger<TokenController> _logger;
        public TokenController(IConfiguration configuration, ILogger<TokenController> logger)
        {
            _configuration = configuration;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("token")]
        public async Task<IActionResult> Post([FromBody]Login loginViewModel)
        {
            try
            {
                var userId = "";
                LoginServices ls = new LoginServices(_configuration);
                var datraasd = await ls.Getloginuser(loginViewModel);
                if (Convert.ToString(datraasd.Tables[0].Rows[0]["messageCode"]) == "1")
                {
                    userId = datraasd.Tables[0].Rows[0]["user_id"].ToString();
                    var claims = new[]
                    {
                            new Claim(JwtRegisteredClaimNames.Sub, loginViewModel.username),
                             new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
                        };
                    var token = new JwtSecurityToken
                    (
                        issuer: _configuration["Issuer"],
                        audience: _configuration["Audience"],
                        claims: claims,
                        expires: DateTime.UtcNow.AddDays(1),
                        notBefore: DateTime.UtcNow,
                        signingCredentials: new SigningCredentials(new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["SigningKey"])),
                             SecurityAlgorithms.HmacSha256)
                    );
                    var encodedJwt = new JwtSecurityTokenHandler().WriteToken(token);

                    datraasd.Tables[0].Columns.Add("access_token");
                    datraasd.Tables[0].Rows[0]["access_token"] = encodedJwt;
                    return Json(datraasd);
                }
                else
                {
                    userId = datraasd.Tables[0].Rows[0]["message"].ToString();
                    if (userId == "Login Failed")
                    {
                        return Json(datraasd);
                    }
                }
                return BadRequest();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        private async Task<string> GetUserIdFromCredentials(Login loginViewModel)
        {
            string userId = "-1";
            LoginServices ls = new LoginServices(_configuration);
            var datra = await ls.Getloginuser(loginViewModel);
            if (Convert.ToString(datra.Tables[0].Rows[0]["messageCode"]) == "1")
            {
                userId = datra.Tables[0].Rows[0]["user_id"].ToString();
            }
            else
            {
                userId = datra.Tables[0].Rows[0]["message"].ToString();
            }
            return userId;
        }
    }
}
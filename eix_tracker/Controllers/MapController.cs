﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eix_tracker.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PusherServer;
//using System.Web.Mvc;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Map")]
    public class MapController : Controller
    {

        private Pusher pusher;
        private readonly ILogger<MapController> _logger;
        //public MapController(ILogger<MapController> logger)
        //{
        //    _logger = logger;
        //}

        public MapController(ILogger<MapController> logger)
        {
            var options = new PusherOptions();
            options.Cluster = "ap2";

            pusher = new Pusher(
                "568169",
                "e0acb5f1de23a88e278f",
                "003e8d270d10a3b71a7d",
                options
            );
            _logger = logger;
        }

        [HttpGet("location_track")]
        public JsonResult Index(string lat, string lng)
        {
            try
            {
                if (lat != null && lng != null)
                {
                    var latitude = lat;
                    var longitude = lng;

                    var location = new
                    {
                        latitude = latitude,
                        longitude = longitude
                    };

                    pusher.TriggerAsync("location_channel", "new_location", location);

                    return Json(new { status = "success", data = location });
                }
                else
                {
                    var location = new
                    {
                        latitude = 23.00,
                        longitude = 72.00
                    };
                    return Json(new { status = "Static", data = location });
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }

        }
    }
}
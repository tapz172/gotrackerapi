﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using eix_tracker.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Rotativa.AspNetCore;
using System.IO;
using Rotativa.AspNetCore.Options;
using MySql.Data.MySqlClient;
using System.Data;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Roles")]
    public class RolesController : Controller
    {
        private readonly ILogger<RolesController> _logger;
        private IConfiguration _config;
        public RolesController(IConfiguration config,ILogger<RolesController> logger)
        {
            _config = config;
            _logger = logger;
        }

        public MySqlConnection connection()
        {
            //string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            string Myconectionstring = this._config.GetValue<string>("ConnectionStrings:DefaultConnection");
            return new MySqlConnection(Myconectionstring);
        }
        public JsonResult index()
        {
            var response = DemoModelPDF();
            return Json(response);
        }

        //[HttpGet("GetallRole_list")]
        public IActionResult DemoModelPDF()
        {
            try
            {
                MemoryStream workStream = new MemoryStream();
                Document document = new Document();
                PdfWriter.GetInstance(document, workStream).CloseStream = false;

                //string Myconectionstring = "server=localhost;database=trackerdb;user=root;password=";
                List<Role> list = new List<Role>();
                using (MySqlConnection conn = connection())
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("sms_user_role", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    using (var reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            list.Add(new Role()
                            {
                                Role_Id = Convert.ToInt16(reader["Role_ID"]).ToString(),
                                Role_Name = reader["Role_Name"].ToString(),
                            });
                        }
                    }
                }
                document.Open();
                document.Add(new Paragraph(list.ToString()));
                document.Add(new Paragraph(DateTime.Now.ToString()));
                document.Close();

                byte[] byteInfo = workStream.ToArray();
                workStream.Write(byteInfo, 0, byteInfo.Length);
                workStream.Position = 0;


                string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
                var filePath = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/pdf/"), uniqueFileName + ".pdf");

                return new ViewAsPdf("DemoModelPDF", list)
                {
                    FileName = uniqueFileName,
                    PageSize = Rotativa.AspNetCore.Options.Size.A4,
                    PageOrientation = Orientation.Portrait,
                    PageMargins = { Left = 0, Right = 0 },
                    SaveOnServerPath = filePath,
                };
                //RoleServices rs = new RoleServices();
                //var response = rs.DemoModelPDF();
                //return Json(response);
                //return new ViewAsPdf(Json(response));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);

                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [HttpGet("GetallRole_list")]
        public FileStreamResult pdf(Role rg)
        {
            MemoryStream workStream = new MemoryStream();
            Document document = new Document();
            PdfWriter.GetInstance(document, workStream).CloseStream = false;


            List<Role> list = new List<Role>();
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_role", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Role()
                        {
                            Role_Id = Convert.ToInt16(reader["role_id"]).ToString(),
                            Role_Name = reader["Role_Name"].ToString(),
                        });
                    }
                }
            }


            document.Open();
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/Receipt.html")))
            {
                body = reader.ReadToEnd();
            }

            foreach (var item in list)
            {
                body = body.Replace("[client_name]", item.Role_Id);
                body = body.Replace("[client_addreess]", item.Role_Name);
                body = body.Replace("[client_mobile]", DateTime.Now.ToString());
            }
            var itemsTable = @"<table><tr><th style=""font-weight: bold"">Item #</th></tr>";
            foreach (var item in list)
                if (item != null)
                {
                    // Each CheckBoxList item has a value of ITEMNAME|ITEM#|QTY, so we split on | and pull these values out...
                    var pieces = item.Role_Id;
                    itemsTable += string.Format("<tr><td>{0}</td></tr>",
                                                pieces);
                }
            itemsTable += "</table>";
            body = body.Replace("[ITEMS]", itemsTable);


            var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(body), null);
            foreach (var htmlElement in parsedHtmlElements)
                document.Add(htmlElement as IElement);
            document.Close();
            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;
            return new FileStreamResult(workStream, "application/pdf");
        }

        public async Task<IActionResult> Download(string filename)
        {
            if (filename == null)
                return Content("filename not present");

            var path = Path.Combine(
                           Directory.GetCurrentDirectory(),
                           "wwwroot", filename);

            var memory = new MemoryStream();
            using (var stream = new FileStream(path, FileMode.Open))
            {
                await stream.CopyToAsync(memory);
            }
            memory.Position = 0;
            return File(memory, Path.GetFileName(path));
        }

        //[Authorize]
        [HttpGet("GetallRole")]
        [ResponseCache(VaryByHeader = "GetallRole", Duration = 60)]
        public JsonResult GetallRoleData()
        {
            try
            {
                RoleServices rs = new RoleServices(_config);
                var response = rs.GetRoles();
                string res = Json(response).ToString();
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[Authorize]
        [HttpGet("Get_user_role_list")]
        //[ResponseCache(VaryByHeader = "Get_user_Role_List", Duration = 100)]
        public JsonResult Get_user_Role_List(Role rl)
        {
            try
            {
                RoleServices rs = new RoleServices(_config);
                var response = rs.Get_user_Role_List(rl);
                string res = Json(response).ToString();
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("role_access")]
        //[ResponseCache(VaryByHeader = "Role_access", Duration = 100)]
        public JsonResult Role_access([FromBody]Role rl)
        {
            try
            {
                RoleServices rs = new RoleServices(_config);
                var response = rs.role_access(rl);
                string res = Json(response).ToString();
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
    }
}

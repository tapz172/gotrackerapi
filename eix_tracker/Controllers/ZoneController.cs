﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Models;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Zone")]
    public class ZoneController : Controller
    {
        private IConfiguration _config;
        private readonly ILogger<ZoneController> _logger;
        public ZoneController(IConfiguration config, ILogger<ZoneController> logger)
        {
            _config = config;
            _logger = logger;
        }

        [Authorize]
        [HttpPost("Add_route_zone")]
        public JsonResult add_route_zone([FromBody]Zone zn)
        {
            try
            {

                ZoneServices zs = new ZoneServices(_config);
                var response = zs.add_route_zone(zn);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("View_route_zone")]
        [ResponseCache(VaryByHeader = "View_route_zone", Duration = 100)]
        public JsonResult view_route_zone(Zone zn)
        {
            try
            {
                ZoneServices zs = new ZoneServices(_config);
                var response = zs.view_route_Zone(zn);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpDelete("delete_route_zone")]
        public JsonResult delete_route_zone(string zoneid,string update_date)
        {
            try
            {
                ZoneServices zs = new ZoneServices(_config);
                var response = zs.delete_route_zone(zoneid, update_date);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Models;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Security_company")]
    public class Security_companyController : Controller
    {
        private IConfiguration _config;
        private readonly ILogger<Security_companyController> _logger;
        public Security_companyController(ILogger<Security_companyController> logger, IConfiguration config)
        {
            _config = config;
            _logger = logger;
        }

        [Authorize]
        [HttpGet("Get_all_users")]
        [ResponseCache(VaryByHeader = "Get_all_users", Duration = 100)]
        public JsonResult Get_All_UserList(register user_List)
        {
            try
            {
                Security_company src = new Security_company(_config);
                var response = src.GetUsers(user_List);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("Add_user_List")]
        public JsonResult Add_User_List([FromBody]register user_List)
        {
            try
            {
                Security_company sdf = new Security_company(_config);
                var response = sdf.AddUser_List(user_List);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("Update_user_List")]
        public JsonResult user_list_update([FromBody]register rg)
        {
            try
            {
                Security_company reg = new Security_company(_config);
                var response = reg.Updateuser_List(rg);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }


        [Authorize]
        [HttpPost("add_location_service")]
        public JsonResult add_location_service([FromBody]add_location_service als)
        {
            try
            {
                Security_company reg = new Security_company(_config);
                var response = reg.sms_add_location_service(als);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPut("update_location_services")]
        public JsonResult update_location_services([FromBody]add_location_service als)
        {
            try
            {
                Security_company reg = new Security_company(_config);
                var response = reg.sms_update_location_services(als);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpDelete("delete_location_service")]
        public JsonResult delete_location_service([FromBody]add_location_service als)
        {
            try
            {
                Security_company reg = new Security_company(_config);
                var response = reg.sms_delete_location_service(als);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("view_location_services")]
        public JsonResult view_location_services(int userid, int roleid ,string start_limit, string search, string search_string)
        {
            try
            {
                Security_company reg = new Security_company(_config);
                var response = reg.sms_view_location_services(userid,roleid,start_limit, search, search_string);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("company_client_list")]
        public JsonResult company_client_list(register rg)
        {
            try
            {
                Security_company reg = new Security_company(_config);
                var response = reg.company_client_list(rg);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Models;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Cors;
using System.Net.Http;
using System.Web;
using System.IO;
using System.Threading;

namespace eix_tracker.Controllers
{
    [EnableCors("CORS")]
    [Produces("application/json")]
    [Route("api/attachment")]
    public class attachmentController : Controller
    {
        private readonly ILogger<attachmentController> _logger;
        private IConfiguration Configuration { get; set; }
        public attachmentController(ILogger<attachmentController> logger, IConfiguration configuration)
        {
            _logger = logger;
            Configuration = configuration;
        }
        public interface IFormFile
        {
            string ContentType { get; }
            string ContentDisposition { get; }
            IHeaderDictionary Headers { get; }
            long Length { get; }
            string Name { get; }
            string FileName { get; }
            Stream OpenReadStream();
            void CopyTo(Stream target);
        }

        [HttpPost("student")]
        public ActionResult Student([FromForm]StudentModel std)
        {

            // Getting Name
            string name = std.Name;
            // Getting Image
            var image = std.Image;
            // Saving Image on Server
            var filePaths = new List<string>();
            if (image.Length > 0)
            {
                var filePath = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/reportimages/"), image.FileName);
                //fileStream.CopyToAsync(fileStream); 
                //var filePath = Path.Combine("wwwroot/NewFolder", image.FileName);
                filePaths.Add(filePath);
                using (var fileStream = new FileStream(filePath, FileMode.Create))
                {
                    image.CopyTo(fileStream);
                }
            }
            return Ok(new { status = true, message = "Student Posted Successfully" });
        }

        [Authorize]
        [HttpPost("add_attachment")]
        public JsonResult user_add_attachment([FromBody]attachmentCollection atcollection)
        {
            try
            {
                attachmentServices ats = new attachmentServices(Configuration);
                var response = ats.add_attachments(atcollection);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("add_attachment_for_andrd_ios")]
        public JsonResult user_add_attachment_for_andrd_ios([FromForm] attachmentfor_andrd_ios std)
        {
            try
            {
                attachmentServices ats = new attachmentServices(Configuration);
                var response = ats.add_attachments_mobile_device(std);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[Authorize]
        [HttpGet("view_attachments")]
        public JsonResult view_attachments(attachment attach)
        {
            try
            {
                attachmentServices atch = new attachmentServices(Configuration);
                var response = atch.view_attachments(attach);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
    }
}
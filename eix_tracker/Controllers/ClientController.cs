﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using eix_tracker.Models;
using eix_tracker.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Controllers
{
    [Produces("application/json")]
    [Route("api/Client")]
    public class ClientController : Controller
    {
        private IConfiguration _config;
        private readonly ILogger<ClientController> _logger;
        public ClientController(IConfiguration config, ILogger<ClientController> logger)
        {
            _config = config;
            _logger = logger;

        }

        [Authorize]
        [HttpPost("Add_Client_location")]
        public JsonResult Add_client_location([FromBody]Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.add_client_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }


        [Authorize]
        [HttpPost("sms_add_payment")]
        public JsonResult sms_add_payment([FromBody]add_payment cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.add_payment(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("sms_add_guard_location")]
        public JsonResult sms_add_guard_location([FromBody]sms_add_guard_location cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.sms_add_guard_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("sms_view_payment")]
        public JsonResult client_based_location(string user_id ,string roleid)
        {
            try
            {
                ClientServices cs = new ClientServices(_config);
                var response = cs.sms_view_payment(user_id, roleid);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        //[Authorize]
        [HttpGet("sms_view_state_tax")]
        public JsonResult sms_view_state_tax(string state_code)
        {
            try
            {
                ClientServices cs = new ClientServices(_config);
                var response = cs.sms_view_state_tax(state_code);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("client_based_location")]
        public JsonResult client_based_location(Client cl)
        {
            try
            {
                ClientServices cs = new ClientServices(_config);
                var response = cs.client_based_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("client_total_location")]
        [ResponseCache(VaryByHeader = "client_total_location", Duration = 100)]
        public JsonResult client_total_location(Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.client_total_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpGet("user_view_location")]
        [ResponseCache(VaryByHeader = "user_view_location", Duration = 100)]
        public JsonResult User_view_location(Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.view_user_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }

        }

        [Authorize]
        [HttpGet("sms_client_identify")]
        public JsonResult sms_client_identify(string user_id)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.sms_client_identify(user_id);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }

        }

        [Authorize]
        [HttpGet("user_location_detail")]
        public JsonResult user_location_detail(Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.user_Location_Details(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpPost("user_location_update")]
        public JsonResult user_location_Location([FromBody]Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.update_user_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }

        [Authorize]
        [HttpDelete("user_location_delete")]
        public JsonResult user_location_delete([FromBody]Client cl)
        {
            try
            {
                ClientServices cls = new ClientServices(_config);
                var response = cls.Delete_User_location(cl);
                return Json(response);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message, new object[0]);
                var content = new ExceptionMessageContent()
                {
                    Error = ex.ToString(),
                    Message = ex.Message.ToString()
                };
                return Json(content);
            }
        }
    }
}
﻿using System;
using eix_tracker.Models;
using System.IO;
using MySql.Data.MySqlClient;
using System.Data;
using Microsoft.Extensions.Configuration;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace eix_tracker.Services
{
    public class ReportServices
    {
        #region Config_services
        private IConfiguration Configuration { get; set; }
        public ReportServices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region connection
        public MySqlConnection connection()
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            return new MySqlConnection(Myconectionstring);
        }
        #endregion

        #region Insert_Report
        public DataSet Insert_Report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                string user = string.Empty;
                string msg = string.Empty;
                string message = string.Empty;
                string UserCode = string.Empty;
                string email_id = string.Empty;
                string user_name = string.Empty;
                string guard_name = string.Empty;

                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_add_report", conn);
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@client_id", rpt.clientid);
                cmd.Parameters.AddWithValue("@location_id", rpt.locationid);
                cmd.Parameters.AddWithValue("@entry_date", rpt.entry_date);
                cmd.Parameters.AddWithValue("@report_type", rpt.report_type);
                cmd.Parameters.AddWithValue("@report_desc", rpt.report_desc);
                cmd.Parameters.AddWithValue("@taskid", rpt.task_id);
                cmd.Parameters.AddWithValue("@lat_upd", rpt.lat_upd);
                cmd.Parameters.AddWithValue("@longitude_upd", rpt.longitude_upd);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@report_detail_id", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@report_detail_id"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@report_id", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@report_id"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@security_company_email", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@security_company_email"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@security_company_username", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@security_company_username"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@guard_user_name", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@guard_user_name"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                    message = (string)cmd.Parameters["@report_id"].Value.ToString();
                    email_id = (string)cmd.Parameters["@security_company_email"].Value.ToString();
                    user_name = (string)cmd.Parameters["@security_company_username"].Value.ToString();
                    guard_name = (string)cmd.Parameters["@guard_user_name"].Value.ToString();
                    user = (string)cmd.Parameters["@checkUser"].Value.ToString();
                    msg = (string)cmd.Parameters["@checkMessage"].Value.ToString();
                }
                if (user == "1" && email_id != null)
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        byte[] bytes = System.Text.Encoding.UTF8.GetBytes(message);
                        string rpt_id = Convert.ToBase64String(bytes);
                        SmtpClient SmtpServer = new SmtpClient();
                        SmtpServer.UseDefaultCredentials = true;
                        mail.To.Add(email_id);
                        mail.From = new MailAddress((this.Configuration.GetValue<string>("Smtp:from")));
                        mail.Subject = "goTracker : " + guard_name + " has added a report";
                        mail.IsBodyHtml = true;
                        string body = string.Empty;
                        using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/report.html")))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{UserName}", "<strong>" + user_name + ",</strong>");
                        var message_title = "Please click the following link to view Report";
                        body = body.Replace("{message_title_html}", message_title);
                        var message_header = "";
                        body = body.Replace("{header}", message_header);
                        var weburl = this.Configuration.GetValue<string>("Smtp:weburl");
                        var message_code = "<a href =" + weburl + "viewReport/" + message + ">Here the view report</a>";
                        body = body.Replace("{Title}", message_code);
                        mail.Body = body;
                        SmtpServer.Host = this.Configuration.GetValue<string>("Smtp:Host");
                        SmtpServer.Port = this.Configuration.GetValue<int>("Smtp:Port");
                        SmtpServer.Credentials = new NetworkCredential(this.Configuration.GetValue<string>("Smtp:UserName"), this.Configuration.GetValue<string>("Smtp:Password"));
                        SmtpServer.EnableSsl = this.Configuration.GetValue<Boolean>("Smtp:EnableSsl");
                        SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        try
                        {
                            SmtpServer.Send(mail);
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                        }
                    }
                    string message123 = string.Empty;
                    switch (message)
                    {
                        case "1":
                            message123 = "Username already exists.\\nPlease choose a different username.";
                            break;
                        case "2":
                            message123 = "Supplied email address has already been used.";
                            break;
                        default:
                            message123 = "Registration successful. Activation email has been sent.";
                            break;
                    }
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region Insert_Report_detail
        public DataSet Insert_Report_detail(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_add_report_detail", conn);
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@reportid", rpt.report_id);
                cmd.Parameters.AddWithValue("@report_desc", rpt.report_desc);
                cmd.Parameters.AddWithValue("@entry_date", rpt.entry_date);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@report_detail_id", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@report_detail_id"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region user_view_report
        public async Task<DataSet> user_view_report(string userid, string roleid, string start_limit, string search, string search_string)
        {
            using (MySqlConnection conn = connection())
            {
                await conn.OpenAsync();

                if (search == "yes" && search != null)
                {  
                    //for web serach
                    MySqlCommand cmd = new MySqlCommand("sms_view_report_search", conn);
                    DataSet ds = new DataSet();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@roleid", roleid);
                    cmd.Parameters.AddWithValue("@start_limit", start_limit);
                    cmd.Parameters.AddWithValue("@search_string", search_string);
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        adp.Fill(ds);
                    }
                    foreach (DataTable tables in ds.Tables)
                    {
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
                else
                {
                    MySqlCommand cmd = new MySqlCommand("sms_view_report", conn);
                    DataSet ds = new DataSet();
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@roleid", roleid);
                    cmd.Parameters.AddWithValue("@start_limit", start_limit);
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        adp.Fill(ds);
                    }
                    foreach (DataTable tables in ds.Tables)
                    {
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }
        }
        #endregion

        #region view_service_question_detail
        public DataSet view_service_question_detail(string questionid)
        {
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_view_service_question_detail", conn);
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@questionid", questionid);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region view_incident_report
        public DataSet view_incident_report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_view_incident_report", conn);
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@roleid", rpt.roleid);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region view_report_detail_indiv
        public DataSet view_report_detail_indiv(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_view_report_detail_indiv", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@report_detail_id", rpt.report_detail_id);
                cmd.Parameters.AddWithValue("@update_date", rpt.entry_date);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region user_report_detail
        public DataSet user_report_detail(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_view_report_detail", conn);
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@reportid", rpt.report_id);
                cmd.CommandType = CommandType.StoredProcedure;
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();

                }
                return ds;
            }
        }
        #endregion

        #region view_report_service_que_answ
        public DataSet view_report_service_que_answ(string report_id)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_view_report_service_que_answ", conn);
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@report_id", report_id);
                cmd.CommandType = CommandType.StoredProcedure;
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    if (tables.Rows.Count > 0)
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion 

        #region add_report_service_que_answ
        public DataSet add_report_service_que_answ(add_report_service_que_answ _add_report_service_que_answ)
        {
            string connection = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn = new MySqlConnection(connection))
            {
                conn.Open();
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("sms_add_report_service_que_answ", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                foreach (services services in _add_report_service_que_answ._services)
                {
                    cmd.Parameters.AddWithValue("@report_id", _add_report_service_que_answ.report_id_answ);
                    cmd.Parameters.AddWithValue("@service_id", services.services_id);
                    cmd.Parameters.AddWithValue("@question_id", services.qus_id);
                    cmd.Parameters.AddWithValue("@question_answ", services.answer);
                    cmd.Parameters.AddWithValue("@answ_comment", services.comments);
                    cmd.Parameters.AddWithValue("@entry_date", services.entry_date);
                    if (services.answ_image == "" || services.answ_image == null)
                    {
                        cmd.Parameters.AddWithValue("@answ_image", "");
                    }
                    else
                    {
                        string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
                        cmd.Parameters.AddWithValue("@answ_image", uniqueFileName + "." + services.img_ext);
                        Byte[] bytes = Convert.FromBase64String(services.answ_image);
                        string fullFileName = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/reportimages/"));
                        File.WriteAllBytes(fullFileName + "\\" + uniqueFileName + "." + services.img_ext, bytes);
                    }
                    if (services.answ_video == "" || services.answ_video == null)
                    {
                        cmd.Parameters.AddWithValue("@answ_video", "");
                    }
                    else
                    {
                        string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
                        cmd.Parameters.AddWithValue("@answ_video", uniqueFileName + "." + services.vdo_ext);
                        Byte[] bytes = Convert.FromBase64String(services.answ_video);
                        string fullFileName = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/reportvideo/"));
                        File.WriteAllBytes(fullFileName + "\\" + uniqueFileName + "." + services.vdo_ext, bytes);
                    }
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        adp.Fill(ds);
                    }
                    cmd.Parameters.Clear();
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        //[HttpGet("get_report_detail_pdf")]
        //public FileStreamResult pdf(Report rg)
        //{
        //    MemoryStream workStream = new MemoryStream();
        //    Document document = new Document();
        //    PdfWriter.GetInstance(document, workStream).CloseStream = false;


        //    List<Report> list = new List<Report>(); 
        //    using (MySqlConnection conn = connection())
        //    {
        //        conn.Open();
        //        MySqlCommand cmd = new MySqlCommand("sms_view_report_detail", conn);
        //        cmd.Parameters.AddWithValue("@reportid", rg.report_id);
        //        cmd.CommandType = CommandType.StoredProcedure;
        //        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
        //        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
        //        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
        //        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
        //        using (var reader = cmd.ExecuteReader())
        //        {
        //            while (reader.Read())
        //            {
        //                list.Add(new Report()
        //                {
        //                    report_id = Convert.ToInt16(reader["report_id"]),
        //                });
        //            }
        //        }
        //    }
        //    document.Open();
        //    string body = string.Empty;
        //    using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/Receipt.html")))
        //    {
        //        body = reader.ReadToEnd();
        //    }

        //    //foreach (var item in list)
        //    //{
        //    //    body = body.Replace("[ORDERID]", item.Role_Id);
        //    //    body = body.Replace("[TOTALPRICE]", item.Role_Name);
        //    //    body = body.Replace("[ORDERDATE]", DateTime.Now.ToString());
        //    //}
        //    var itemsTable = @"<table><tr><th style=""font-weight: bold"">Item #</th></tr>";
        //    foreach (var item in list)
        //        if (item != null)
        //        {
        //            // Each CheckBoxList item has a value of ITEMNAME|ITEM#|QTY, so we split on | and pull these values out...
        //            var pieces = item.report_desc;
        //            itemsTable += string.Format("<tr><td>{0}</td></tr>",
        //                                        pieces);
        //        }
        //    itemsTable += "</table>";
        //    body = body.Replace("[ITEMS]", itemsTable);


        //    var parsedHtmlElements = HTMLWorker.ParseToList(new StringReader(body), null);
        //    foreach (var htmlElement in parsedHtmlElements)
        //        document.Add(htmlElement as IElement);
        //    document.Close();
        //    byte[] byteInfo = workStream.ToArray();
        //    workStream.Write(byteInfo, 0, byteInfo.Length);
        //    workStream.Position = 0;
        //    return new FileStreamResult(workStream, "application/pdf");
        //}

        #region user_delete_report
        public DataSet user_delete_report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_delete_report", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@reportid", rpt.report_id);
                cmd.Parameters.AddWithValue("@update_date", rpt.entry_date);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region user_delete_report_detail
        public DataSet user_delete_report_detail(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_delete_report_detail", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@reportdetailid", rpt.report_detail_id);
                cmd.Parameters.AddWithValue("@update_date", rpt.entry_date);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region user_report_update
        public DataSet user_report_update(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_update_report", conn);
                conn.Open();
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@reportdetailid", rpt.report_detail_id);
                cmd.Parameters.AddWithValue("@reportdesc", rpt.report_desc);
                cmd.Parameters.AddWithValue("@update_date", rpt.entry_date);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region total_security_comp
        public DataSet total_security_comp()
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_total_security_comp", conn);
                conn.Open();
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_total_report
        public DataSet sms_total_report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_total_report", conn);
                conn.Open();
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@roleid", rpt.roleid);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_total_incident_report
        public DataSet sms_total_incident_report(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_total_incident_report", conn);
                conn.Open();
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                cmd.Parameters.AddWithValue("@roleid", rpt.roleid);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_total_users
        public DataSet sms_total_users()
        {
            try
            {
                using (MySqlConnection conn = connection())
                {
                    string user = string.Empty;
                    MySqlCommand cmd = new MySqlCommand("sms_total_users", conn);
                    conn.Open();
                    DataSet ds = new DataSet();
                    cmd.CommandType = CommandType.StoredProcedure;
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        adp.Fill(ds);
                    }
                    foreach (DataTable tables in ds.Tables)
                    {
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }
            catch (Exception ex)
            {
                throw (ex);
            }
        }
        #endregion

        #region sms_total_client_manager_guard
        public DataSet sms_total_client_manager_guard(Report rpt)
        {
            using (MySqlConnection conn = connection())
            {
                MySqlCommand cmd = new MySqlCommand("sms_total_client_manager_guard", conn);
                conn.Open();
                DataSet ds = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", rpt.user_id);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_client_location_based_services
        public DataSet sms_client_location_based_services(int client_id, int location_id)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("sms_client_location_based_services", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@client_id", client_id);
                    cmd.Parameters.AddWithValue("@location_id", location_id);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion
    }
}

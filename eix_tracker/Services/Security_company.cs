﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace eix_tracker.Services
{
    public class Security_company
    {
        #region Config_services
        private IConfiguration Configuration { get; set; }
        public Security_company(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region Insert_User_List
        public DataSet AddUser_List(register user_List)
        {
            string user = string.Empty;
            string msg = string.Empty;
            string message = string.Empty;
            string UserCode = string.Empty;
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            MySqlCommand cmd = new MySqlCommand("sms_add_user", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@comp_id", user_List.Comp_ID);
            cmd.Parameters.AddWithValue("@username", user_List.username);
            cmd.Parameters.AddWithValue("@first_name", user_List.Firstname);
            cmd.Parameters.AddWithValue("@last_name", user_List.Lastname);
            cmd.Parameters.AddWithValue("@emailaddr", user_List.Email_ID);
            cmd.Parameters.AddWithValue("@phone_no", user_List.Phone_No);
            cmd.Parameters.AddWithValue("@password_has", user_List.Password);
            cmd.Parameters.AddWithValue("@role_id", user_List.role_Id);
            cmd.Parameters.AddWithValue("@entry_date", user_List.Entry_Date);
            cmd.Parameters.AddWithValue("@address_one", user_List.Address_One);
            cmd.Parameters.AddWithValue("@address_two", user_List.Address_Two);
            cmd.Parameters.AddWithValue("@city", user_List.City);
            cmd.Parameters.AddWithValue("@state", user_List.State);
            cmd.Parameters.AddWithValue("@zip", user_List.Zip);
            cmd.Parameters.AddWithValue("@client_identify", user_List.client_identify);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@profile_insert_id", MySqlDbType.VarChar, 500);
                cmd.Parameters["@profile_insert_id"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@random_num", MySqlDbType.VarChar, 500);
                cmd.Parameters["@random_num"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
                con.Open();
                message = (string)cmd.Parameters["@profile_insert_id"].Value.ToString();
                UserCode = (string)cmd.Parameters["@random_num"].Value.ToString();
                user = (string)cmd.Parameters["@checkUser"].Value.ToString();
                msg = (string)cmd.Parameters["@checkMessage"].Value.ToString();
                con.Close();
            }
            if (user == "1" && user_List.Email_ID != null)
            {
                using (MailMessage mail = new MailMessage())
                {
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(message);
                    string user_id = Convert.ToBase64String(bytes);

                    byte[] bytesone = System.Text.Encoding.UTF8.GetBytes(UserCode);
                    string user_code = Convert.ToBase64String(bytesone);

                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.UseDefaultCredentials = true;
                    mail.To.Add(user_List.Email_ID);
                    mail.From = new MailAddress((this.Configuration.GetValue<string>("Smtp:from")));
                    mail.Subject = "Account Activation";
                    mail.IsBodyHtml = true;
                    //string body = "Hello <h4>" + user_List.username + "</h4>";
                    string body = string.Empty;
                    using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/forgotpassword.html")))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("{UserName}", "<strong>" + user_List.Firstname + ",</strong>");
                    body = body.Replace("{email}", "<strong>" + user_List.Email_ID + "</strong>");
                    body = body.Replace("{password}", "<strong>" + user_List.Password + "</strong>");
                    var message_title = "Please click the following link to activate your account";
                    body = body.Replace("{message_title_html}", message_title);
                    var message_header = "";
                    body = body.Replace("{header}", message_header);
                    var url = this.Configuration.GetValue<string>("Smtp:testurl");
                    var message_code = "<a href =" + url + "verification.html?Key=" + user_code + "&USER_ID=" + user_id + ">Click here to activate your account</a>";
                    body = body.Replace("{Title}", message_code);
                    mail.Body = body;
                    SmtpServer.Host = this.Configuration.GetValue<string>("Smtp:Host");
                    SmtpServer.Port = this.Configuration.GetValue<int>("Smtp:Port"); ;
                    SmtpServer.Credentials = new NetworkCredential(this.Configuration.GetValue<string>("Smtp:UserName"), this.Configuration.GetValue<string>("Smtp:Password"));
                    SmtpServer.EnableSsl = this.Configuration.GetValue<Boolean>("Smtp:EnableSsl");
                    SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    try
                    {
                        SmtpServer.Send(mail);
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
                //SendEmail();
                string message123 = string.Empty;
                switch (message)
                {
                    case "1":
                        message123 = "Username already exists.\\nPlease choose a different username.";
                        break;
                    case "2":
                        message123 = "Supplied email address has already been used.";
                        break;
                    default:
                        message123 = "Registration successful. Activation email has been sent.";
                        break;
                }
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region Get_All_Users
        public DataSet GetUsers(register user_List)
        {
            if (user_List.search == "yes" && user_List.search != null)
            {
                string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
                List<Role> list = new List<Role>();
                using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("sms_company_user_list_search", conn);
                    DataSet ds = new DataSet();
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@userid", user_List.user_id);
                        cmd.Parameters.AddWithValue("@start_limit", user_List.start_limit);
                        cmd.Parameters.AddWithValue("@search_string", user_List.search_string);
                        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        adp.Fill(ds);
                    }
                    foreach (DataTable table in ds.Tables)
                    {
                        table.TableName = table.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }
            else
            {
                string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
                List<Role> list = new List<Role>();
                using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
                {
                    conn.Open();
                    MySqlCommand cmd = new MySqlCommand("sms_company_user_list", conn);
                    DataSet ds = new DataSet();
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@userid", user_List.user_id);
                        cmd.Parameters.AddWithValue("@start_limit", user_List.start_limit);
                        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        adp.Fill(ds);
                    }
                    foreach (DataTable table in ds.Tables)
                    {
                        table.TableName = table.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }
            
        }
        #endregion

        #region company_client_list
        public DataSet company_client_list(register rg)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<Role> list = new List<Role>();
            using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_company_client_list", conn);
                DataSet ds = new DataSet();
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", rg.user_id);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable table in ds.Tables)
                {
                    table.TableName = table.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region Update_user_List
        public DataSet Updateuser_List(register rg)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("sms_company_update_user_profile", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", rg.user_id);
                    cmd.Parameters.AddWithValue("@firstname", rg.Firstname);
                    cmd.Parameters.AddWithValue("@lastname", rg.Lastname);
                    cmd.Parameters.AddWithValue("@phoneno", rg.Phone_No);
                    cmd.Parameters.AddWithValue("@addressone", rg.Address_One);
                    cmd.Parameters.AddWithValue("@addresstwo", rg.Address_Two);
                    cmd.Parameters.AddWithValue("@city", rg.City);
                    cmd.Parameters.AddWithValue("@state", rg.State);
                    cmd.Parameters.AddWithValue("@zip", rg.Zip);
                    cmd.Parameters.AddWithValue("@update_date", rg.Update_Date);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_add_location_service
        public DataSet sms_add_location_service(add_location_service als)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("sms_add_location_service", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", als.userid);
                    cmd.Parameters.AddWithValue("@locationid", als.locationid);
                    cmd.Parameters.AddWithValue("@service_name", als.service_name);
                    cmd.Parameters.AddWithValue("@service_status", als.service_status);
                    cmd.Parameters.AddWithValue("@entry_date", als.entry_date);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_view_location_services
        public DataSet sms_view_location_services(int userid, int roleid, string start_limit, string search, string search_string)
        {
            if (search == "yes" && search != null)
            {
                string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
                using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
                {
                    conn1.Open();
                    DataSet ds = new DataSet();
                    MySqlCommand cmd = new MySqlCommand("sms_view_location_services_search", conn1);
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@userid", userid);
                        cmd.Parameters.AddWithValue("@roleid", roleid);
                        cmd.Parameters.AddWithValue("@start_limit", start_limit);
                        cmd.Parameters.AddWithValue("@search_string", search_string);
                        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        adp.Fill(ds);
                    }
                    foreach (DataTable tables in ds.Tables)
                    {
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }
            else
            {
                string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
                using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
                {
                    conn1.Open();
                    DataSet ds = new DataSet();
                    MySqlCommand cmd = new MySqlCommand("sms_view_location_services", conn1);
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@userid", userid);
                        cmd.Parameters.AddWithValue("@roleid", roleid);
                        cmd.Parameters.AddWithValue("@start_limit", start_limit);
                        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        adp.Fill(ds);
                    }
                    foreach (DataTable tables in ds.Tables)
                    {
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }
           
        }
        #endregion

        #region sms_update_location_services
        public DataSet sms_update_location_services(add_location_service als)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("sms_update_location_services", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@serviceid", als.serviceid);
                    cmd.Parameters.AddWithValue("@service_name", als.service_name);
                    cmd.Parameters.AddWithValue("@service_status", als.service_status);
                    cmd.Parameters.AddWithValue("@location_id", als.locationid);
                    cmd.Parameters.AddWithValue("@update_date", als.entry_date);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_delete_location_service
        public DataSet sms_delete_location_service(add_location_service als)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("sms_delete_location_service", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@serviceid", als.serviceid);
                    cmd.Parameters.AddWithValue("@update_date", als.entry_date);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion
    }
}
﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Services
{
    public class ApplicationDbContext
    {
        #region Connection_Settings
        public string ConnectionStrings { get; set; }

        public ApplicationDbContext(string connectionString)
        {
            this.ConnectionStrings = connectionString;
        }

        private MySqlConnection GetConnection()
        {
            return new MySqlConnection(ConnectionStrings);
        }
        #endregion
    }
}

﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using eix_tracker.Data;
using System.IO;
using Microsoft.Extensions.Configuration;

namespace eix_tracker.Services
{
    public class attachmentServices
    {
        #region Config_services
        private IConfiguration Configuration { get; set; }
        public attachmentServices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region _add_attachments
        public DataSet add_attachments(attachmentCollection atcollection)
        {
            string connection = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn = new MySqlConnection(connection))
            {
                conn.Open();
                DataSet ds = new DataSet();
                DataTable mydatatable = new DataTable();
                foreach (attachment attachment in atcollection.arrAttachment)
                {
                    ds.Clear();
                    MySqlCommand cmd = new MySqlCommand("sms_add_attachments", conn);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@reportdetailid", attachment.report_detail_id);
                    cmd.Parameters.AddWithValue("@attachment_type", attachment.attachment_type);
                    if (attachment.attachment_type == "Image")
                    {
                        string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
                        cmd.Parameters.AddWithValue("@attachment_name", uniqueFileName + "." + attachment.ext);
                        Byte[] bytes = Convert.FromBase64String(attachment.attachment_name);
                        string fullFileName = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/reportimages/"));
                        File.WriteAllBytes(fullFileName + "\\" + uniqueFileName + "." + attachment.ext, bytes);
                        //cmd.Parameters.AddWithValue("@attachment_name", attch.attachment_name);
                    }
                    else if (attachment.attachment_type == "Video")
                    {
                        string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
                        cmd.Parameters.AddWithValue("@attachment_name", uniqueFileName + "." + attachment.ext);
                        Byte[] bytes = Convert.FromBase64String(attachment.attachment_name);
                        string fullFileName = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/reportvideo/"));
                        File.WriteAllBytes(fullFileName + "\\" + uniqueFileName + "." + attachment.ext, bytes);
                    }
                    else
                    {
                        string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
                        cmd.Parameters.AddWithValue("@attachment_name", uniqueFileName + "." + attachment.ext);
                        Byte[] bytes = Convert.FromBase64String(attachment.attachment_name);
                        string fullFileName = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/reports_other/"));
                        File.WriteAllBytes(fullFileName + "\\" + uniqueFileName + "." + attachment.ext, bytes);
                    }
                    cmd.Parameters.AddWithValue("@entry_date", attachment.entry_date);
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                    {
                        cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        adp.Fill(ds);
                    }
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region _add_attachments_mobile_device
        public DataSet add_attachments_mobile_device(attachmentfor_andrd_ios std)
        {
            string connection = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn = new MySqlConnection(connection))
            {
                conn.Open();
                DataSet ds = new DataSet();
                DataTable mydatatable = new DataTable();
                MySqlCommand cmd = new MySqlCommand("sms_add_attachments", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@reportdetailid", std.report_detail_id);

                // Getting Image
                var image = std.File;
                if (std.attachment_type == "Image")
                {
                    // Saving Image on Server
                    var filePaths = new List<string>();
                    if (image.Length > 0)
                    {
                        var filePath = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/reportimages/"), image.FileName);
                        filePaths.Add(filePath);
                        //image.CopyToAsync(uniqueFileName + "." + "png");
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            image.CopyTo(fileStream);
                        }
                    }
                    cmd.Parameters.AddWithValue("@attachment_name", image.FileName);
                    cmd.Parameters.AddWithValue("@entry_date", std.entry_date);
                    cmd.Parameters.AddWithValue("@attachment_type", std.attachment_type);
                }
                else if (std.attachment_type == "Video")
                {
                    // Saving Image on Server
                    var filePaths = new List<string>();
                    if (image.Length > 0)
                    {
                        var filePath = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/reportvideo/"), image.FileName);
                        filePaths.Add(filePath);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            image.CopyTo(fileStream);
                        }
                    }
                    cmd.Parameters.AddWithValue("@attachment_name", image.FileName);
                    cmd.Parameters.AddWithValue("@entry_date", std.entry_date);
                    cmd.Parameters.AddWithValue("@attachment_type", std.attachment_type);
                }
                else
                {
                    // Saving Image on Server
                    var filePaths = new List<string>();
                    if (image.Length > 0)
                    {
                        var filePath = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/reports_other/"), image.FileName);
                        filePaths.Add(filePath);
                        using (var fileStream = new FileStream(filePath, FileMode.Create))
                        {
                            image.CopyTo(fileStream);
                        }
                    }
                    cmd.Parameters.AddWithValue("@attachment_name", image.FileName);
                    cmd.Parameters.AddWithValue("@entry_date", std.entry_date);
                    cmd.Parameters.AddWithValue("@attachment_type", "Other");
                }
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion


        #region view_attachments
        public DataSet view_attachments(attachment attach)
        {
            string connection = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn = new MySqlConnection(connection))
            {
                MySqlCommand cmd = new MySqlCommand("sms_view_attachments", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                DataSet ds = new DataSet();
                cmd.Parameters.AddWithValue("@report_detail_id", attach.report_detail_id);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

    }
}

﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace eix_tracker.Models
{
    public class LoginServices
    {
        #region Config_services
        private IConfiguration Configuration { get; set; }
        public LoginServices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region GetUserlogins
        public async Task<DataSet> Getloginuser(Login loginViewModel)
        {
            string user = string.Empty;
            string msg = string.Empty;
            string gt_id = string.Empty;
            string role_id = string.Empty;
            string login_attempt_failed = string.Empty;
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            await con.OpenAsync();
            MySqlCommand cmd1 = new MySqlCommand("sms_user_profile_login", con);
            DataSet dt = new DataSet();
            cmd1.CommandType = CommandType.StoredProcedure;
            cmd1.Parameters.AddWithValue("@emailaddr", loginViewModel.username);
            cmd1.Parameters.AddWithValue("@password_p", loginViewModel.password);
            cmd1.Parameters.AddWithValue("@public_ip", loginViewModel.public_ip);
            cmd1.Parameters.AddWithValue("@device_id", loginViewModel.device_id);
            cmd1.Parameters.AddWithValue("@browser_desc", loginViewModel.browser_desc);
            cmd1.Parameters.AddWithValue("@os_desc", loginViewModel.os_desc);
            cmd1.Parameters.AddWithValue("@lat_upd", loginViewModel.lat_upd);
            cmd1.Parameters.AddWithValue("@longitude_upd", loginViewModel.longitude_upd);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
            {
                cmd1.Parameters.Add("@get_user_id", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@get_user_id"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@get_role_id", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@get_role_id"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@loginattempt", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@loginattempt"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@get_register_date", MySqlDbType.DateTime, 500);
                cmd1.Parameters["@get_register_date"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@get_subscription_status", MySqlDbType.Int16, 500);
                cmd1.Parameters["@get_subscription_status"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@get_close_date", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@get_close_date"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@get_last_date", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@get_last_date"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@get_plan_amount", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@get_plan_amount"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@get_plan_desc", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@get_plan_desc"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@get_total_guard", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@get_total_guard"].Direction = ParameterDirection.Output;
                cmd1.Parameters.Add("@get_first_payment_status", MySqlDbType.VarChar, 500);
                cmd1.Parameters["@get_first_payment_status"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
                gt_id = (string)cmd1.Parameters["@get_user_id"].Value.ToString();
                role_id = (string)cmd1.Parameters["@get_role_id"].Value.ToString();
                user = (string)cmd1.Parameters["@checkUser"].Value.ToString();
                msg = (string)cmd1.Parameters["@checkMessage"].Value.ToString();
                if (user == "0")
                {
                    login_attempt_failed = (string)cmd1.Parameters["@loginattempt"].Value.ToString();
                }
            }
            await cmd1.ExecuteNonQueryAsync();
            con.Close();
            foreach (DataTable table in dt.Tables)
            {
                table.TableName = table.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region Login Attempt
        public DataSet Login_Attempt(Login_Attempt lga)
        {
            string user = string.Empty;
            string UserCode = string.Empty;
            string UserName = string.Empty;
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_check_login_attempt", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@emailaddr", lga.email_id);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@random_num", MySqlDbType.VarChar, 500);
                cmd.Parameters["@random_num"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@get_first_name", MySqlDbType.VarChar, 500);
                cmd.Parameters["@get_first_name"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
                user = (string)cmd.Parameters["@checkUser"].Value.ToString();
                UserCode = (string)cmd.Parameters["@random_num"].Value.ToString();
                UserName = (string)cmd.Parameters["@get_first_name"].Value.ToString();
                if (user == "1" && lga.email_id != null)
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        SmtpClient SmtpServer = new SmtpClient();
                        SmtpServer.UseDefaultCredentials = true;
                        mail.To.Add(lga.email_id);
                        mail.From = new MailAddress((this.Configuration.GetValue<string>("Smtp:from")));
                        mail.Subject = "Your one-time passcode to confirm password reset";
                        mail.IsBodyHtml = true;
                        string body = string.Empty;
                        using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/forgotpassword.html")))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{UserName}", UserName + ",");
                        var message_title = "You attempted incorrect password many times. Please use pass-code.";
                        var message_header = "Pass-Code";
                        body = body.Replace("{header}", message_header);
                        body = body.Replace("{message_title_html}", message_title);
                        body = body.Replace("{Title}", UserCode);
                        mail.Body = body;
                        SmtpServer.Host = this.Configuration.GetValue<string>("Smtp:Host");
                        SmtpServer.Port = this.Configuration.GetValue<int>("Smtp:Port"); ;
                        SmtpServer.Credentials = new NetworkCredential(this.Configuration.GetValue<string>("Smtp:UserName"), this.Configuration.GetValue<string>("Smtp:Password"));
                        SmtpServer.EnableSsl = this.Configuration.GetValue<Boolean>("Smtp:EnableSsl");
                        SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        try
                        {
                            SmtpServer.Send(mail);
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                        }
                    }
                }
                con.Close();
            }
            foreach (DataTable table in dt.Tables)
            {
                table.TableName = table.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        public Login Compareuser(string username, string password_p)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<Login> list = new List<Login>();
            using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_profile_login", conn);
                cmd.CommandType = CommandType.StoredProcedure;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Login()
                        {
                            username = username,
                            password = password_p,
                        });
                    }
                }
            }
            return null;
        }
        public List<Login> Userloginvalues()
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");

            List<Login> list = new List<Login>();


            using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_profile_login", conn);

                cmd.CommandType = CommandType.StoredProcedure;
                using (var reader = cmd.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        list.Add(new Login()
                        {
                            username = reader["username"].ToString(),
                            password = reader["password_p"].ToString(),
                        });
                    }
                }
            }
            return list;
        }
    }

}


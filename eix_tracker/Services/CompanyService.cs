﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace eix_tracker.Services
{
    public class CompanyService
    {
        #region Config_services
        private IConfiguration Configuration { get; set; }
        public CompanyService(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region DisplayCompany
        public DataSet display_company(string start_limit, string search, string search_string)
        {
            if (search == "yes" && search != null)
            {
                string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
                using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
                {
                    string user = string.Empty;
                    string msg = string.Empty;
                    string compname = string.Empty;
                    conn1.Open();
                    MySqlCommand cmd1 = new MySqlCommand("sms_comp_list_search", conn1);
                    cmd1.Parameters.AddWithValue("@start_limit", start_limit);
                    cmd1.Parameters.AddWithValue("@search_string", search_string);
                    DataSet ds = new DataSet();
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                    {
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        //cmd1.ExecuteNonQuery();
                        adp.Fill(ds);
                    }
                    foreach (DataTable tables in ds.Tables)
                    {
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }
            else
            {
                string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
                using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
                {
                    string user = string.Empty;
                    string msg = string.Empty;
                    string compname = string.Empty;
                    conn1.Open();
                    MySqlCommand cmd1 = new MySqlCommand("sms_comp_list", conn1);
                    cmd1.Parameters.AddWithValue("@start_limit", start_limit);
                    DataSet ds = new DataSet();
                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                    {
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        //cmd1.ExecuteNonQuery();
                        adp.Fill(ds);
                    }
                    foreach (DataTable tables in ds.Tables)
                    {
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }

        }
        #endregion

        #region sms_add_service_question
        public DataSet sms_add_service_question(sms_add_service_question adsq)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                string user = string.Empty;
                string msg = string.Empty;
                string compname = string.Empty;
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("sms_add_service_question", conn1);
                DataSet ds = new DataSet();

                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@userid", adsq.userid);
                    cmd1.Parameters.AddWithValue("@service_id", adsq.service_id);
                    cmd1.Parameters.AddWithValue("@question_text", adsq.question_text);
                    cmd1.Parameters.AddWithValue("@question_status", adsq.question_status);
                    cmd1.Parameters.AddWithValue("@entry_date", adsq.entry_date);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    //cmd1.ExecuteNonQuery();
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_view_service_question
        public DataSet sms_view_service_question(int userid, int roleid, string start_limit, string search, string search_string)
        {
            if (search == "yes" && search != null)
            {
                string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
                using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
                {

                    string user = string.Empty;
                    string msg = string.Empty;
                    string compname = string.Empty;
                    conn1.Open();
                    MySqlCommand cmd1 = new MySqlCommand("sms_view_service_question_search", conn1);
                    DataSet ds = new DataSet();

                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                    {
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@userid", userid);
                        cmd1.Parameters.AddWithValue("@roleid", roleid);
                        cmd1.Parameters.AddWithValue("@start_limit", start_limit);
                        cmd1.Parameters.AddWithValue("@search_string", search_string);
                        cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        //cmd1.ExecuteNonQuery();
                        adp.Fill(ds);
                    }
                    foreach (DataTable tables in ds.Tables)
                    {
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }
            else
            {
                string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
                using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
                {

                    string user = string.Empty;
                    string msg = string.Empty;
                    string compname = string.Empty;
                    conn1.Open();
                    MySqlCommand cmd1 = new MySqlCommand("sms_view_service_question", conn1);
                    DataSet ds = new DataSet();

                    using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                    {
                        cmd1.CommandType = CommandType.StoredProcedure;
                        cmd1.Parameters.AddWithValue("@userid", userid);
                        cmd1.Parameters.AddWithValue("@roleid", roleid);
                        cmd1.Parameters.AddWithValue("@start_limit", start_limit);
                        cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                        cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                        cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                        cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                        //cmd1.ExecuteNonQuery();
                        adp.Fill(ds);
                    }
                    foreach (DataTable tables in ds.Tables)
                    {
                        tables.TableName = tables.Rows[0]["TableName"].ToString();
                    }
                    return ds;
                }
            }

        }
        #endregion

        #region sms_update_service_question
        public DataSet sms_update_service_question(sms_add_service_question adsq)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                string user = string.Empty;
                string msg = string.Empty;
                string compname = string.Empty;
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("sms_update_service_question", conn1);
                DataSet ds = new DataSet();

                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@questionid", adsq.questionid);
                    cmd1.Parameters.AddWithValue("@service_id", adsq.service_id);
                    cmd1.Parameters.AddWithValue("@question_text", adsq.question_text);
                    cmd1.Parameters.AddWithValue("@question_status", adsq.question_status);
                    cmd1.Parameters.AddWithValue("@update_date", adsq.entry_date);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    //cmd1.ExecuteNonQuery();
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_subscription_status_change
        public DataSet sms_subscription_status_change(subscription_status_change adsq)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("sms_subscription_status_change", conn1);
                DataSet ds = new DataSet();
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@userid", adsq.userid);
                    cmd1.Parameters.AddWithValue("@sub_status", adsq.sub_status);
                    cmd1.Parameters.AddWithValue("@close_date", adsq.close_date);
                    cmd1.Parameters.AddWithValue("@last_date", adsq.last_date);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    //cmd1.ExecuteNonQuery();
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_delete_service_question
        public DataSet sms_delete_service_question(sms_add_service_question adsq)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                string user = string.Empty;
                string msg = string.Empty;
                string compname = string.Empty;
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("sms_delete_service_question", conn1);
                DataSet ds = new DataSet();

                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@questionid", adsq.questionid);
                    cmd1.Parameters.AddWithValue("@update_date", adsq.entry_date);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    //cmd1.ExecuteNonQuery();
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion
    }
}

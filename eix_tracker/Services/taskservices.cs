﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;


namespace eix_tracker.Services
{
    public class taskservices
    {

        #region Config_services
        private IConfiguration Configuration { get; set; }
        public taskservices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region ADD_USER_TASK
        public DataSet add_user_task(TaskManage tm)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_add_task", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@userid", tm.userid);
            cmd.Parameters.AddWithValue("@clientid", tm.clientid);
            cmd.Parameters.AddWithValue("@locationid", tm.locationid);
            cmd.Parameters.AddWithValue("@service_id", tm.serviceId);
            cmd.Parameters.AddWithValue("@task_name", tm.Task_Name);
            cmd.Parameters.AddWithValue("@task_description", tm.Description);
            cmd.Parameters.AddWithValue("@task_status", tm.task_status);
            cmd.Parameters.AddWithValue("@task_frequency", tm.task_frequency);
            cmd.Parameters.AddWithValue("@task_assigment", tm.Task_Assigment);
            cmd.Parameters.AddWithValue("@entry_date", tm.entry_date);
            cmd.Parameters.AddWithValue("@task_time_duration", tm.task_time_duration);
            cmd.Parameters.AddWithValue("@allocate_to", tm.allocate_to);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                adp.Fill(dt);

            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region View_user_task
        public DataSet View_user_task(TaskManage tm)
        {
            if (tm.search == "yes" && tm.search != null)
            {
                string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
                MySqlConnection con = new MySqlConnection(Myconectionstring);
                con.Open();
                MySqlCommand cmd = new MySqlCommand("sms_view_task_search", con);
                DataSet dt = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", tm.userid);
                cmd.Parameters.AddWithValue("@roleid", tm.roleid);
                cmd.Parameters.AddWithValue("@start_limit", tm.start_limit);
                cmd.Parameters.AddWithValue("@search_string", tm.search_string);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(dt);
                }
                foreach (DataTable tables in dt.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return dt;
            }
            else
            {
                string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
                MySqlConnection con = new MySqlConnection(Myconectionstring);
                con.Open();
                MySqlCommand cmd = new MySqlCommand("sms_view_task", con);
                DataSet dt = new DataSet();
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@userid", tm.userid);
                cmd.Parameters.AddWithValue("@roleid", tm.roleid);
                cmd.Parameters.AddWithValue("@start_limit", tm.start_limit);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(dt);
                }
                foreach (DataTable tables in dt.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return dt;
            }


        }
        #endregion

        #region View_task_details
        public DataSet View_task_details(TaskManage tm)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_task_detail", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@taskid", tm.taskid);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region update_user_task
        public DataSet update_user_task(TaskManage tm)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_update_task", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@taskid", tm.taskid);
            cmd.Parameters.AddWithValue("@task_name", tm.Task_Name);
            cmd.Parameters.AddWithValue("@task_description", tm.Description);
            cmd.Parameters.AddWithValue("@task_frequency", tm.task_frequency);
            cmd.Parameters.AddWithValue("@task_assigment", tm.Task_Assigment);
            cmd.Parameters.AddWithValue("@update_date", tm.entry_date);
            cmd.Parameters.AddWithValue("@task_time_duration", tm.task_time_duration);
            cmd.Parameters.AddWithValue("@allocate_to", tm.allocate_to);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region delete_task_details
        public DataSet delete_task_details(TaskManage tm)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            DataSet dt = new DataSet();
            MySqlCommand cmd = new MySqlCommand("sms_delete_task", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@taskid", tm.taskid);
            cmd.Parameters.AddWithValue("@update_date", tm.entry_date);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region sms_all_user_list
        public DataSet sms_all_user_list(string userid)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("sms_all_user_list", conn1);
                DataSet ds = new DataSet();
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@userid", userid);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_location_based_services
        public DataSet sms_location_based_services(int location_id)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            DataSet dt = new DataSet();
            MySqlCommand cmd = new MySqlCommand("sms_location_based_services", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@location_id", location_id);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region sms_services_based_questions
        public DataSet sms_services_based_questions(int service_id)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            DataSet dt = new DataSet();
            MySqlCommand cmd = new MySqlCommand("sms_services_based_questions", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@service_id", service_id);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region sms_view_services_detail
        public DataSet sms_view_services_detail(int servicesid)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            DataSet dt = new DataSet();
            MySqlCommand cmd = new MySqlCommand("sms_view_services_detail", con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@servicesid", servicesid);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion
    }
}

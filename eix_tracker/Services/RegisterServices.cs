﻿using eix_tracker.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Mail;
using System.Threading.Tasks;

namespace eix_tracker.Services
{
    public class RegisterServices
    {
        #region Config_services
        private IConfiguration Configuration { get; set; }
        public RegisterServices(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region InsertUser
        public DataSet RegisterUser(register rg)
        {
            string user = string.Empty;
            string msg = string.Empty;
            string message = string.Empty;
            string UserCode = string.Empty;
            //int userId = 0;
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            MySqlCommand cmd = new MySqlCommand("sms_user_profile_register", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@user_name", rg.username);
            cmd.Parameters.AddWithValue("@first_name", rg.Firstname);
            cmd.Parameters.AddWithValue("@last_name", rg.Lastname);
            cmd.Parameters.AddWithValue("@emailaddr", rg.Email_ID);
            cmd.Parameters.AddWithValue("@phone_no", rg.Phone_No);
            cmd.Parameters.AddWithValue("@password_has", rg.Password);
            cmd.Parameters.AddWithValue("@role_id", rg.role_Id);
            cmd.Parameters.AddWithValue("@entry_date", rg.Entry_Date);
            cmd.Parameters.AddWithValue("@comp_name", rg.Comp_Name);
            cmd.Parameters.AddWithValue("@comp_type", rg.Comp_Type);
            cmd.Parameters.AddWithValue("@address_one", rg.Address_One);
            cmd.Parameters.AddWithValue("@address_two", rg.Address_Two);
            cmd.Parameters.AddWithValue("@city", rg.City);
            cmd.Parameters.AddWithValue("@state", rg.State);
            cmd.Parameters.AddWithValue("@plan_amount", rg.plan_amount);
            cmd.Parameters.AddWithValue("@plan_desc", rg.plan_desc);
            cmd.Parameters.AddWithValue("@zip", rg.Zip);
            cmd.Parameters.AddWithValue("@license_state", rg.license_state);
            cmd.Parameters.AddWithValue("@dps_no", rg.dps_no);
            cmd.Parameters.AddWithValue("@lat", rg.lat);
            cmd.Parameters.AddWithValue("@longitude", rg.longitude);
            cmd.Parameters.AddWithValue("@total_guard", rg.total_guard);
            string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
            if (rg.pic == "")
            {
                cmd.Parameters.AddWithValue("@pic", "");
            }
            else
            {
                cmd.Parameters.AddWithValue("@pic", uniqueFileName + "." + rg.ext);
                Byte[] bytes = Convert.FromBase64String(rg.pic);
                string fullFileName = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images/"));
                File.WriteAllBytes(fullFileName + "\\" + uniqueFileName + "." + rg.ext, bytes);
            }
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@profile_insert_id", MySqlDbType.VarChar, 500);
                cmd.Parameters["@profile_insert_id"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@random_num", MySqlDbType.VarChar, 500);
                cmd.Parameters["@random_num"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
                con.Open();
                message = (string)cmd.Parameters["@profile_insert_id"].Value.ToString();
                UserCode = (string)cmd.Parameters["@random_num"].Value.ToString();
                user = (string)cmd.Parameters["@checkUser"].Value.ToString();
                msg = (string)cmd.Parameters["@checkMessage"].Value.ToString();
                con.Close();
            }
            if (user == "1" && rg.Email_ID != null)
            {
                using (MailMessage mail = new MailMessage())
                {
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(message);
                    string user_id = Convert.ToBase64String(bytes);

                    byte[] bytesone = System.Text.Encoding.UTF8.GetBytes(UserCode);
                    string user_code = Convert.ToBase64String(bytesone);

                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.UseDefaultCredentials = true;
                    mail.To.Add(rg.Email_ID);
                    mail.From = new MailAddress((this.Configuration.GetValue<string>("Smtp:from")));
                    mail.Subject = "goTracker : " + "Registration activation";
                    mail.IsBodyHtml = true;
                    string body = string.Empty;
                    using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/forgotpassword.html")))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("{UserName}", "<strong>" + rg.Firstname + ",</strong>");
                    body = body.Replace("{email}", "<strong>" + rg.Email_ID + "</strong>");
                    body = body.Replace("{password}", "<strong>" + rg.Password + "</strong>");
                    var message_title = "Please click the following link to activate your account";
                    body = body.Replace("{message_title_html}", message_title);
                    var message_header = "";
                    body = body.Replace("{header}", message_header);
                    var url = this.Configuration.GetValue<string>("Smtp:testurl");
                    var message_code = "<a href =" + url + "verification.html?Key=" + user_code + "&USER_ID=" + user_id + ">Click here to activate your account</a>";
                    body = body.Replace("{Title}", message_code);
                    mail.Body = body;
                    SmtpServer.Host = this.Configuration.GetValue<string>("Smtp:Host");
                    SmtpServer.Port = this.Configuration.GetValue<int>("Smtp:Port"); ;
                    SmtpServer.Credentials = new NetworkCredential(this.Configuration.GetValue<string>("Smtp:UserName"), this.Configuration.GetValue<string>("Smtp:Password"));
                    SmtpServer.EnableSsl = this.Configuration.GetValue<Boolean>("Smtp:EnableSsl");
                    SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    try
                    {
                        SmtpServer.Send(mail);
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
                //SendEmail();
                string message123 = string.Empty;
                switch (message)
                {
                    case "1":
                        message123 = "Username already exists.\\nPlease choose a different username.";
                        break;
                    case "2":
                        message123 = "Supplied email address has already been used.";
                        break;
                    default:
                        message123 = "Registration successful. Activation email has been sent.";
                        break;
                }
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region sms_user_email_verify_generate
        public DataSet sms_user_email_verify_generate(string Email_ID)
        {
            string user = string.Empty;
            string msg = string.Empty;
            string message = string.Empty;
            string UserCode = string.Empty;
            string username = string.Empty;
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            MySqlCommand cmd = new MySqlCommand("sms_user_email_verify_generate", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@emailaddr", Email_ID);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@get_random_num", MySqlDbType.VarChar, 500);
                cmd.Parameters["@get_random_num"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@get_user_id", MySqlDbType.VarChar, 500);
                cmd.Parameters["@get_user_id"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@get_username", MySqlDbType.VarChar, 500);
                cmd.Parameters["@get_username"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
                con.Open();
                message = (string)cmd.Parameters["@get_user_id"].Value.ToString();
                UserCode = (string)cmd.Parameters["@get_random_num"].Value.ToString();
                user = (string)cmd.Parameters["@checkUser"].Value.ToString();
                msg = (string)cmd.Parameters["@checkMessage"].Value.ToString();
                username = (string)cmd.Parameters["@get_username"].Value.ToString();
                con.Close();
            }
            if (user == "1" && Email_ID != null)
            {
                using (MailMessage mail = new MailMessage())
                {
                    byte[] bytes = System.Text.Encoding.UTF8.GetBytes(message);
                    string user_id = Convert.ToBase64String(bytes);

                    byte[] bytesone = System.Text.Encoding.UTF8.GetBytes(UserCode);
                    string user_code = Convert.ToBase64String(bytesone);

                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.UseDefaultCredentials = true;
                    mail.To.Add(Email_ID);
                    mail.From = new MailAddress((this.Configuration.GetValue<string>("Smtp:from")));
                    mail.Subject = "goTracker : " + "Registration activation";
                    mail.IsBodyHtml = true;
                    string body = string.Empty;
                    using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/forgotpassword_new.html")))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("{UserName}", "<strong>" + username + ",</strong>");
                    var message_title = "Please click the following link to activate your account";
                    body = body.Replace("{message_title_html}", message_title);
                    var message_header = "Pass-Code";
                    body = body.Replace("{header}", message_header);
                    var url = this.Configuration.GetValue<string>("Smtp:testurl");
                    var message_code = "<a href =" + url + "verification.html?Key=" + user_code + "&USER_ID=" + user_id + ">Click here to activate your account</a>";
                    body = body.Replace("{Title}", message_code);
                    mail.Body = body;
                    SmtpServer.Host = this.Configuration.GetValue<string>("Smtp:Host");
                    SmtpServer.Port = this.Configuration.GetValue<int>("Smtp:Port"); ;
                    SmtpServer.Credentials = new NetworkCredential(this.Configuration.GetValue<string>("Smtp:UserName"), this.Configuration.GetValue<string>("Smtp:Password"));
                    SmtpServer.EnableSsl = this.Configuration.GetValue<Boolean>("Smtp:EnableSsl");
                    SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    try
                    {
                        SmtpServer.Send(mail);
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
                //SendEmail();
                string message123 = string.Empty;
                switch (message)
                {
                    case "1":
                        message123 = "Username already exists.\\nPlease choose a different username.";
                        break;
                    case "2":
                        message123 = "Supplied email address has already been used.";
                        break;
                    default:
                        message123 = "Registration successful. Activation email has been sent.";
                        break;
                }
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        protected void SendEmail()
        {
            register RG = new register();
            string body = this.PopulateBody(RG);
            this.SendHtmlFormattedEmail(RG);
        }

        private string PopulateBody(register rg)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/forgotpassword.html")))
            {
                body = reader.ReadToEnd();
            }
            body = body.Replace("{UserName}", rg.username);
            body = body.Replace("{Title}", rg.Firstname);
            body = body.Replace("{Url}", rg.Lastname);
            body = body.Replace("{Description}", rg.ImageContent);
            return body;
        }
        private void SendHtmlFormattedEmail(register rg)
        {
            using (MailMessage mail = new MailMessage())
            {
                SmtpClient SmtpServer = new SmtpClient();
                SmtpServer.UseDefaultCredentials = true;
                mail.To.Add("tapan.bhatt@eixsys.com");
                mail.From = new MailAddress("tapan.bhatt@eixsys.com");
                mail.Subject = "Your one-time passcode to confirm password reset";
                mail.IsBodyHtml = true;

                string body = string.Empty;
                using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/forgotpassword.html")))
                {
                    body = reader.ReadToEnd();
                }
                body = body.Replace("{UserName}", rg.username);
                body = body.Replace("{Title}", rg.Firstname);
                body = body.Replace("{Url}", rg.Lastname);
                body = body.Replace("{Description}", rg.ImageContent);
                body += "<br /><br />use the passcode to register";
                body += "<br /><h1> 456677  </h1>";
                body += "<br /><br />Thanks";
                mail.Body = body;
                SmtpServer.Host = "smtp.office365.com";
                SmtpServer.Port = 587;
                SmtpServer.Credentials = new NetworkCredential("tapan.bhatt@eixsys.com", "Tapan172");
                SmtpServer.EnableSsl = true;
                SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                try
                {
                    SmtpServer.Send(mail);
                }
                catch (Exception ex)
                {
                    ex.Message.ToString();
                }
            }
        }


        #region Forgot_Password
        public DataSet Forgot_password(register rg)
        {
            string UserCode = string.Empty;
            string message_code = string.Empty;
            string username = string.Empty;
            string email = string.Empty;
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            MySqlConnection con = new MySqlConnection(Myconectionstring);
            con.Open();
            MySqlCommand cmd = new MySqlCommand("sms_user_forgot_password", con);
            DataSet dt = new DataSet();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@emailaddr", rg.Email_ID);
            using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@random_num", MySqlDbType.VarChar, 500);
                cmd.Parameters["@random_num"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@get_username", MySqlDbType.VarChar, 500);
                cmd.Parameters["@get_username"].Direction = ParameterDirection.Output;
                cmd.Parameters.Add("@get_email", MySqlDbType.VarChar, 500);
                cmd.Parameters["@get_email"].Direction = ParameterDirection.Output;
                adp.Fill(dt);
                UserCode = (string)cmd.Parameters["@checkUser"].Value.ToString();
                message_code = (string)cmd.Parameters["@random_num"].Value.ToString();
                username = (string)cmd.Parameters["@get_username"].Value.ToString();
                email = (string)cmd.Parameters["@get_email"].Value.ToString();
            }
            if (UserCode == "1" && email != null)
            {
                using (MailMessage mail = new MailMessage())
                {
                    SmtpClient SmtpServer = new SmtpClient();
                    SmtpServer.UseDefaultCredentials = true;
                    mail.To.Add(email);
                    mail.From = new MailAddress((this.Configuration.GetValue<string>("Smtp:from")));
                    mail.Subject = "goTracker : " + "Password Reset Request";
                    mail.IsBodyHtml = true;
                    string body = string.Empty;
                    using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/forgotpassword_new.html")))
                    {
                        body = reader.ReadToEnd();
                    }
                    body = body.Replace("{UserName}", username + ",");
                    var message_title = "You recently requested a new password. To complete your request,Please use pass-code.";
                    var message_header = "Pass-Code";
                    body = body.Replace("{header}", message_header);
                    body = body.Replace("{message_title_html}", message_title);
                    body = body.Replace("{Title}", message_code);
                    mail.Body = body;
                    SmtpServer.Host = this.Configuration.GetValue<string>("Smtp:Host");
                    SmtpServer.Port = this.Configuration.GetValue<int>("Smtp:Port"); ;
                    SmtpServer.Credentials = new NetworkCredential(this.Configuration.GetValue<string>("Smtp:UserName"), this.Configuration.GetValue<string>("Smtp:Password"));
                    SmtpServer.EnableSsl = this.Configuration.GetValue<Boolean>("Smtp:EnableSsl");
                    SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                    try
                    {
                        SmtpServer.Send(mail);
                    }
                    catch (Exception ex)
                    {
                        ex.Message.ToString();
                    }
                }
            }
            foreach (DataTable tables in dt.Tables)
            {
                tables.TableName = tables.Rows[0]["TableName"].ToString();
            }
            return dt;
        }
        #endregion

        #region DisplayUserByID
        public DataSet user_view_profile(register rg)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<registerbyID> list = new List<registerbyID>();
            DataSet ds = new DataSet();
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                string user = string.Empty;
                string msg = string.Empty;
                string compname = string.Empty;
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("sms_user_view_profile", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@userid", rg.user_id);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
            }
            return ds;
        }
        #endregion

        #region DeleteUser
        public DataSet Delete_User(register rg)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<registerbyID> list = new List<registerbyID>();
            DataSet ds = new DataSet();
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                string user = string.Empty;
                string msg = string.Empty;
                string compname = string.Empty;
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("sms_delete_user", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@userid", rg.user_id);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
            }
            return ds;
        }
        #endregion

        #region GET_ALL_USER
        public DataSet Get_All_user(register user)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<register> list = new List<register>();
            using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_view_profile", conn);
                DataSet ds = new DataSet();
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", user.user_id);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable table in ds.Tables)
                {
                    table.TableName = table.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region User_Verify
        public DataSet user_Verify(string user_id, string user_key)
        {
            string UserCode = string.Empty;
            string planprice = string.Empty;
            string totalguard = string.Empty;
            string username = string.Empty;
            string email = string.Empty;
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<registerbyID> list = new List<registerbyID>();
            DataSet ds = new DataSet();
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("sms_user_verify", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@userid", user_id);
                    cmd1.Parameters.AddWithValue("@random_num", user_key);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@get_username", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@get_username"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@get_user_email", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@get_user_email"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@get_plan_price", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@get_plan_price"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@get_total_guard", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@get_total_guard"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                    UserCode = (string)cmd1.Parameters["@checkUser"].Value.ToString();
                    planprice = (string)cmd1.Parameters["@get_plan_price"].Value.ToString();
                    totalguard = (string)cmd1.Parameters["@get_total_guard"].Value.ToString();
                    username = (string)cmd1.Parameters["@get_username"].Value.ToString();
                    email = (string)cmd1.Parameters["@get_user_email"].Value.ToString();
                }
                if (UserCode == "1" && email != null)
                {
                    using (MailMessage mail = new MailMessage())
                    {
                        SmtpClient SmtpServer = new SmtpClient();
                        SmtpServer.UseDefaultCredentials = true;
                        mail.To.Add(email);
                        mail.From = new MailAddress((this.Configuration.GetValue<string>("Smtp:from")));
                        mail.Subject = "goTracker : " + "Plan Details";
                        mail.IsBodyHtml = true;
                        string body = string.Empty;
                        using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/plandetail.html")))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{UserName}", username + ",");
                        var message_title = "Thank you For Register";
                        body = body.Replace("{message_title_html}", message_title);
                        body = body.Replace("{Title}", planprice);
                        body = body.Replace("{guards}", totalguard);
                        mail.Body = body;
                        SmtpServer.Host = this.Configuration.GetValue<string>("Smtp:Host");
                        SmtpServer.Port = this.Configuration.GetValue<int>("Smtp:Port"); ;
                        SmtpServer.Credentials = new NetworkCredential(this.Configuration.GetValue<string>("Smtp:UserName"), this.Configuration.GetValue<string>("Smtp:Password"));
                        SmtpServer.EnableSsl = this.Configuration.GetValue<Boolean>("Smtp:EnableSsl");
                        SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        try
                        {
                            SmtpServer.Send(mail);
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                        }
                    }
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
            }
            return ds;
        }
        #endregion

        #region sms_change_status
        public DataSet sms_change_status(changestatus cs)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<registerbyID> list = new List<registerbyID>();
            DataSet ds = new DataSet();
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("sms_change_status", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@userid", cs.userid);
                    cmd1.Parameters.AddWithValue("@change_status", cs.change_status);
                    cmd1.Parameters.AddWithValue("@inactive_date", cs.inactive_date);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
            }
            return ds;
        }
        #endregion


        #region total_users_payment
        public DataSet total_users_payment(string userid)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<registerbyID> list = new List<registerbyID>();
            DataSet ds = new DataSet();
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                MySqlCommand cmd1 = new MySqlCommand("total_users_payment", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd1))
                {
                    cmd1.CommandType = CommandType.StoredProcedure;
                    cmd1.Parameters.AddWithValue("@userid", userid);
                    cmd1.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd1.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd1.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
            }
            return ds;
        }
        #endregion

        #region Update_user_Profile
        public DataSet Updateuser_Profile(register rg)
        {

            string uniqueFileName = DateTime.Now.ToString("yyyyMMddHHmmssfffff");
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("sms_user_update_profile", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", rg.user_id);
                    cmd.Parameters.AddWithValue("@firstname", rg.Firstname);
                    cmd.Parameters.AddWithValue("@lastname", rg.Lastname);
                    cmd.Parameters.AddWithValue("@phoneno", rg.Phone_No);
                    cmd.Parameters.AddWithValue("@compname", rg.Comp_Name);
                    cmd.Parameters.AddWithValue("@addressone", rg.Address_One);
                    cmd.Parameters.AddWithValue("@addresstwo", rg.Address_Two);
                    cmd.Parameters.AddWithValue("@city", rg.City);
                    cmd.Parameters.AddWithValue("@state", rg.State);
                    cmd.Parameters.AddWithValue("@zip", rg.Zip);
                    cmd.Parameters.AddWithValue("@licensestate", rg.license_state);
                    cmd.Parameters.AddWithValue("@dpsno", rg.dps_no);
                    cmd.Parameters.AddWithValue("@roleid", rg.role_id);
                    string ImageUpdate = rg.imageUpdate;
                    if (ImageUpdate == "Yes")
                    {
                        //fullpath = null;
                        cmd.Parameters.AddWithValue("@pic", rg.Image);
                    }
                    else if (ImageUpdate == "")
                    {
                        cmd.Parameters.AddWithValue("@pic", "");
                    }
                    else
                    {
                        cmd.Parameters.AddWithValue("@pic", uniqueFileName + "." + rg.ext);
                        Byte[] bytes = Convert.FromBase64String(rg.Image);
                        string fullFileName = Path.Combine(System.IO.Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images/"));
                        File.WriteAllBytes(fullFileName + "\\" + uniqueFileName + "." + rg.ext, bytes);
                    }
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region Reset_pass
        public DataSet Reset_user(register rg)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("sms_user_reset_password", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@emailaddr", rg.Email_ID);
                    cmd.Parameters.AddWithValue("@password_has", rg.Password);
                    cmd.Parameters.AddWithValue("@random_num", rg.rndnumber);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                    cmd.ExecuteNonQuery();
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region Change_password
        public DataSet Change_password(register rg)
        {

            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            using (MySqlConnection conn1 = new MySqlConnection(Myconectionstring))
            {
                conn1.Open();
                string UserCode = string.Empty;
                string message_code = string.Empty;
                string username = string.Empty;
                string email = string.Empty;
                DataSet ds = new DataSet();
                MySqlCommand cmd = new MySqlCommand("sms_user_change_password", conn1);
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", rg.user_id);
                    cmd.Parameters.AddWithValue("@currentPass", rg.Password);
                    cmd.Parameters.AddWithValue("@newpass", rg.New_Password);
                    cmd.Parameters.AddWithValue("@user_password_status", rg.user_password_status);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@get_user_email", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@get_user_email"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@get_user_name", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@get_user_name"].Direction = ParameterDirection.Output;

                    adp.Fill(ds);
                    cmd.ExecuteNonQuery();
                    UserCode = (string)cmd.Parameters["@checkUser"].Value.ToString();
                    username = (string)cmd.Parameters["@get_user_name"].Value.ToString();
                    email = (string)cmd.Parameters["@get_user_email"].Value.ToString();
                }
                if (UserCode == "1" && rg.user_password_status == "security_company" && email != null)
                {
                    //SendEmail();
                    using (MailMessage mail = new MailMessage())
                    {
                        SmtpClient SmtpServer = new SmtpClient();
                        SmtpServer.UseDefaultCredentials = true;
                        mail.To.Add(email);
                        mail.From = new MailAddress((this.Configuration.GetValue<string>("Smtp:from")));
                        mail.Subject = "goTracker : " + "Super admin has changed your password";
                        mail.IsBodyHtml = true;
                        string body = string.Empty;
                        using (StreamReader reader = new StreamReader(System.IO.Path.Combine("wwwroot/Templates/Template/forgotpassword_new.html")))
                        {
                            body = reader.ReadToEnd();
                        }
                        body = body.Replace("{UserName}", username + ",");
                        var message_title = "Super admin has changed your password";
                        var message_header = "New Password";
                        body = body.Replace("{header}", message_header);
                        body = body.Replace("{message_title_html}", message_title);
                        body = body.Replace("{Title}", rg.New_Password);
                        mail.Body = body;
                        SmtpServer.Host = this.Configuration.GetValue<string>("Smtp:Host");
                        SmtpServer.Port = this.Configuration.GetValue<int>("Smtp:Port"); ;
                        SmtpServer.Credentials = new NetworkCredential(this.Configuration.GetValue<string>("Smtp:UserName"), this.Configuration.GetValue<string>("Smtp:Password"));
                        SmtpServer.EnableSsl = this.Configuration.GetValue<Boolean>("Smtp:EnableSsl");
                        SmtpServer.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
                        try
                        {
                            SmtpServer.Send(mail);
                        }
                        catch (Exception ex)
                        {
                            ex.Message.ToString();
                        }
                    }
                }
                foreach (DataTable tables in ds.Tables)
                {
                    tables.TableName = tables.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_check_email
        public DataSet sms_check_email(string emailaddr)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<register> list = new List<register>();
            using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_check_email", conn);
                DataSet ds = new DataSet();
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@emailaddr", emailaddr);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable table in ds.Tables)
                {
                    table.TableName = table.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion

        #region sms_user_update_status
        public DataSet sms_user_update_status(string userid, string change_status)
        {
            string Myconectionstring = this.Configuration.GetValue<string>("ConnectionStrings:DefaultConnection");
            List<register> list = new List<register>();
            using (MySqlConnection conn = new MySqlConnection(Myconectionstring))
            {
                conn.Open();
                MySqlCommand cmd = new MySqlCommand("sms_user_update_status", conn);
                DataSet ds = new DataSet();
                using (MySqlDataAdapter adp = new MySqlDataAdapter(cmd))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@userid", userid);
                    cmd.Parameters.AddWithValue("@change_status", change_status);
                    cmd.Parameters.Add("@checkUser", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkUser"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@checkMessage", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@checkMessage"].Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@get_current_status", MySqlDbType.VarChar, 500);
                    cmd.Parameters["@get_current_status"].Direction = ParameterDirection.Output;
                    adp.Fill(ds);
                }
                foreach (DataTable table in ds.Tables)
                {
                    table.TableName = table.Rows[0]["TableName"].ToString();
                }
                return ds;
            }
        }
        #endregion
    }
}